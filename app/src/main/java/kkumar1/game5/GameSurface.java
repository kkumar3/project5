package kkumar1.game5;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Karan on 4/1/2017.
 */

public class GameSurface extends SurfaceView implements SurfaceHolder.Callback {

    public static final int WIDTH = 1024;
    public static final int HEIGHT = 530;
    private MainThread thread;
    private Background bg;

    public GameSurface(Context context) {
        super(context);
        getHolder().addCallback(this);
        thread = new MainThread(getHolder(), this);

        setFocusable(true);
    }

    @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            boolean retry = true;
            while(retry)
            {
                try{
                    thread.setRunning(false);
                    thread.join();
                } catch (Exception e){e.printStackTrace();}
                retry = false;
            }
        }

        public void surfaceCreated(SurfaceHolder holder) {

            bg = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.background));
            bg.setVector(-5);
            thread.setRunning(true);
            thread.start();
        }

        public boolean onTouchEvent(MotionEvent event) {
            return super.onTouchEvent(event);
        }

        public void update(){

            bg.update();
        }

        @Override
        public void draw(Canvas canvas){

            final float scaleFactorX = getWidth()/WIDTH;
            final float scaleFactorY = getHeight()/HEIGHT;

            if(canvas!=null){
                final int savedState = canvas.save();
                canvas.scale(scaleFactorX, scaleFactorY );
                bg.draw(canvas);
                canvas.restoreToCount(savedState);
            }
        }
}
