package kkumar1.game5;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

/**
 * Created by Karan on 4/1/2017.
 */

public class MainThread extends Thread {

    private int FPS = 30;
    private SurfaceHolder surfaceHolder;
    private GameSurface gameSurface;
    private boolean running;
    public static Canvas canvas;

    public MainThread(SurfaceHolder surfaceholder, GameSurface gameSurface) {
        super();
        this.surfaceHolder = surfaceholder;
        this.gameSurface = gameSurface;

    }

    @Override
    public void run() {


        long startTime;
        long runningTime;
        long waitTime;
        long targetTime = 1000 / FPS;
        while (running) {
            startTime = System.nanoTime();
            canvas = null;
            try {

                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder) {
                    this.gameSurface.update();
                    this.gameSurface.draw(canvas);
                }
            } catch (Exception e) {
            }
            finally{
                if(canvas!=null){
                    try{surfaceHolder.unlockCanvasAndPost(canvas);}
                    catch(Exception e) {e.printStackTrace();}
                }
            }
            runningTime = (System.nanoTime() - startTime) / 1000000;
            waitTime = targetTime - runningTime;

            try {
                sleep(waitTime);
            } catch (Exception e) {
            }
            System.out.println(waitTime);
        }
    }

    public void setRunning(boolean b){
        running=b;
    }
}
